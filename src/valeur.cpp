#include "valeur.hpp"

Valeur::Valeur(double nombre, std::string const &nom) : _nombre(nombre), etudiant(nom) {}

const double Valeur::getNombre() const {
    return _nombre;
}

void Valeur::setNombre(double nombre) {
    _nombre = nombre;
}

double Valeur::getNote() const {
    return _nombre;
}

const std::string & Valeur::getEtudiant() const {
    return etudiant;
}

void Valeur::setNote(double note) {
    _nombre = note;
}

void Valeur::setEtudiant(const std::string & e) {
    etudiant = e;
}