#include "classe.hpp"

Classe::Classe(double inf,double sup) : _inf(inf), _sup(sup), _quantite(0.0) {}

double Classe::getBorneInf() const {
    return _inf;
}

void Classe::setBorneInf(double inf) {
    _inf = inf;
}

double Classe::getBorneSup() const {
    return _sup;
}

void Classe::setBorneSup(double sup) {
    _sup = sup;
}

double Classe::getQuantite() const {
    return _quantite;
}

void Classe::setQuantite(double quantite) {
    _quantite = quantite;
}

void Classe::ajouter() {
    _quantite++;
}

Classe & Classe::operator=(const Classe & c) {
    _inf = c._inf;
    _sup = c._sup;
    _quantite = c._quantite;

    return *this;
}

bool operator<(const Classe & c1, const Classe & c2) {
    return c1.getBorneInf() < c2.getBorneInf();
}

bool operator>(const Classe & c1, const Classe & c2) {
    return c1.getBorneInf() > c2.getBorneInf();
}