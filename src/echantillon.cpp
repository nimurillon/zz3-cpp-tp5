#include "echantillon.hpp"
#include <algorithm>
#include <stdexcept>

int Echantillon::getTaille() const {
    return vec.size();
}

void Echantillon::ajouter(double value) {
    vec.push_back(Valeur(value));
}

const Valeur& Echantillon::getMinimum() const {
    if(!getTaille()) {
        throw std::domain_error("Echantillon vide");
    }
    return *(std::min_element(vec.begin(), vec.end(), [] (Valeur v1, Valeur v2) -> bool {
        return v1.getNombre() < v2.getNombre();
    }));
}

const Valeur& Echantillon::getMaximum() const {
    if(!getTaille()) {
        throw std::domain_error("Echantillon vide");
    }
    return *(std::max_element(vec.begin(), vec.end(), [] (Valeur v1, Valeur v2) -> bool {
        return v1.getNombre() < v2.getNombre();
    }));
}

const Valeur& Echantillon::operator[](int index) const{

    if(index<0 || index>=getTaille()){
        throw std::out_of_range("Can't access to element");
    }

    return vec[index];
}

const Valeur & Echantillon::getValeur(int index) const {
    if(index<0 || index>=getTaille()){
        throw std::out_of_range("Can't access to element");
    }

    return vec[index];
}

Echantillon::const_iterator Echantillon::begin() const {
    return vec.begin();
}

Echantillon::const_iterator Echantillon::end() const {
    return vec.end();
}