#ifndef VALEUR
#define VALEUR

#include <string>

class Valeur {
    double _nombre;
    std::string etudiant;

    public:
        Valeur(double=0.0, std::string const & = "inconnu");
        const double getNombre() const;
        void setNombre(double);
        double getNote() const;
        const std::string& getEtudiant() const;
        void setNote(double);
        void setEtudiant(const std::string &);
};

#endif