#ifndef HISTO
#define HISTO

#include "classe.hpp"
#include "echantillon.hpp"
#include "comparateur.hpp"
#include <vector>
#include <set>
#include <functional>
#include <iostream>
#include <map>

/* class Histogramme {
    double _inf;
    double _sup;
    double _step;
    // std::vector<Classe> vec;
    std::set<Classe> set;

    public:
        Histogramme(double,double,int);
        // std::vector<Classe> & getClasses();
        std::set<Classe> & getClasses();
        void ajouter(Echantillon);
}; */

template <typename COMP = std::less<Classe>>
class Histogramme
{
    double _inf;
    double _sup;
    double _step;
    std::set<Classe, COMP> set;
    std::multimap<Classe, Valeur> map;

public:
    template <typename> friend class Histogramme;

    Histogramme(double, double, int);
    template <typename T2>
    Histogramme(Histogramme<T2> const & h2);
    const std::set<Classe, COMP> &getClasses() const;
    void ajouter(double);
    const std::multimap<Classe, Valeur> & getValeurs() const;
    auto getValeurs(const Classe&) const;
};

template <typename COMP>
Histogramme<COMP>::Histogramme(double inf, double sup, int class_number) : _inf(inf), _sup(sup), _step((sup - inf) / class_number)
{
    for (int i = 0; i < class_number; i++)
    {
        set.insert(Classe(_inf + i * _step, _inf + (i + 1) * _step));
    }
}

template <typename COMP>
template <typename T2>
Histogramme<COMP>::Histogramme(Histogramme<T2> const & h2) : _inf(h2._inf), _sup(h2._sup), _step(h2._step) {
    for(const Classe c : h2.set) {
        set.insert(c);
    }
}

template <typename COMP>
const std::set<Classe, COMP> &Histogramme<COMP>::getClasses() const
{
    return set;
}

template <typename COMP>
void Histogramme<COMP>::ajouter(double d)
{
    auto it = std::find_if(set.begin(), set.end(), [d] (Classe const & c) { return c.getBorneInf() <= d && c.getBorneSup() > d;});

    if (it != set.end())
    {
        auto c = *it;   // Copie de l'élément pointé
        set.erase(it);  // Suppression de l'ancien
        c.ajouter();    // Modification
        set.insert(c);  // Réajout

        map.insert(std::make_pair(c, Valeur(d)));
    }
}

template <typename COMP>
const std::multimap<Classe, Valeur> & Histogramme<COMP>::getValeurs() const {
    return map;
}

template <typename COMP>
auto Histogramme<COMP>::getValeurs(const Classe & c) const {
    return std::equal_range(map.begin(), map.end(), c);
}

bool operator<(const std::pair<Classe, Valeur> & p1, const Classe& c) {
    return p1.first.getBorneInf() < c.getBorneInf();
}

bool operator<(const Classe& c, const std::pair<Classe, Valeur> & p1) {
    return p1.first.getBorneInf() > c.getBorneInf();
}

/* bool operator==(const std::pair<Classe, Valeur> & p1, const Classe& c) {
    return p1.first.getBorneInf() == Approx(c.getBorneInf());
}

bool operator==(const Classe& c, const std::pair<Classe, Valeur> & p1) {
    return p1.first.getBorneInf() == Approx(c.getBorneInf());
} */

#endif