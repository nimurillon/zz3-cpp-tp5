//#include "histogramme.hpp"

/* Histogramme::Histogramme(double inf, double sup, int class_number) : _inf(inf), _sup(sup), _step((sup - inf) / class_number) {
    for(int i=0; i<class_number; i++) {
        vec.push_back(Classe(_inf + i*_step, _inf + (i+1)*_step));
    }
} */

/* std::vector<Classe> & Histogramme::getClasses() {
    return vec;
} */

/* void Histogramme::ajouter(Echantillon e) {
    for(const Valeur &v : e) {
        vec[(v.getNombre()-_inf)/_step].ajouter();
    }
} */

Histogramme::Histogramme(double inf, double sup, int class_number) : _inf(inf), _sup(sup), _step((sup - inf) / class_number) {
    for(int i=0; i<class_number; i++) {
        set.insert(Classe(_inf + i*_step, _inf + (i+1)*_step));
    }
}

std::set<Classe> & Histogramme::getClasses() {
    return set;
}

void Histogramme::ajouter(Echantillon e) {
    std::set<Classe>::iterator it;
    std::set<Classe>::iterator item;
    int classe_idx;

    for(const Valeur &v : e) {
        it = set.begin();
        classe_idx = (int) (v.getNombre()-_inf)/_step;
        std::advance(it, classe_idx);
        Classe c(*it);
        c.ajouter();
        set.erase(it);
        set.insert(c);
    }
}