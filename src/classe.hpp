#ifndef CLASSE
#define CLASSE

class Classe {
    double _inf;
    double _sup;
    double _quantite;

    public:
        Classe(double,double);
        double getBorneInf() const;
        void setBorneInf(double);
        double getBorneSup() const;
        void setBorneSup(double);
        double getQuantite() const;
        void setQuantite(double);
        void ajouter();
        Classe & operator=(const Classe &);
};

bool operator<(const Classe &, const Classe &);
bool operator>(const Classe &, const Classe &);

#endif