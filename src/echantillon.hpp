#ifndef ECHANTILLON
#define ECHANTILLON

#include "valeur.hpp"
#include <vector>

class Echantillon {
    std::vector<Valeur> vec;
    using const_iterator=std::vector<Valeur>::const_iterator;
    public:
        int getTaille() const;
        void ajouter(double);
        const Valeur& getMinimum() const;
        const Valeur& getMaximum() const;
        const Valeur& operator[](int) const;
        const Valeur& getValeur(int) const;
        const_iterator begin() const;
        const_iterator end() const;
};

#endif