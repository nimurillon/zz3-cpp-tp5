#ifndef COMPARATEUR
#define COMPARATEUR

template<typename T>
class ComparateurQuantite {
    public:
        bool operator()(const T & t1, const T & t2) {
            return (t1.getQuantite() > t2.getQuantite() || (t1.getQuantite() == t2.getQuantite() && t1.getBorneInf() < t2.getBorneInf()));
        }
};

#endif